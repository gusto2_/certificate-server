package eu.sse.certserver.caprovider;

import eu.sse.certserver.common.services.CaProvider;
import java.io.InputStreamReader;
import java.security.Security;
import java.security.cert.X509Certificate;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Gabriel
 */
public class CaProviderTest {

    private static final Logger logger = LoggerFactory.getLogger(CaProviderTest.class);

    private static final String KEYSTORE_URL = "file:./target/test-classes/keystores/testrootca.jks";
    private static final String KEYSTORE_PASSWORD = "aStupid123";
    private static final String KEYSTORE_TYPE = "JKS";
    private static final String KEYSTORE_KEY_ALIAS = "testrootca";
    private static final String KEYSTORE_KEY_PASSWORD = "aStupid123";

    private static final String DN_PREFIX = "C=BE";
    private static final String CRL_URL = "https://localhost:9443/cxf/ca/crl/CaServer.crl";

    private CaProviderImpl caProvider;

    @org.junit.Before
    public void setup() {
        
        Security.addProvider(new BouncyCastleProvider());
        
        this.caProvider = new CaProviderImpl();
        // initialize the provider
        this.caProvider.setKeystoreUrl(KEYSTORE_URL);
        this.caProvider.setKeystorePassword(KEYSTORE_PASSWORD);
        this.caProvider.setKeystoreType(KEYSTORE_TYPE);
        this.caProvider.setKeyAlias(KEYSTORE_KEY_ALIAS);
        this.caProvider.setKeyAliasPasswpord(KEYSTORE_KEY_PASSWORD);
        this.caProvider.setSubjectPrefix(DN_PREFIX);
        this.caProvider.setCrlUrl(CRL_URL);
        
        this.caProvider.init();

    }

    @org.junit.Test
    public void testCertificate() throws Exception {
        org.junit.Assert.assertNotNull(this.caProvider);
        X509Certificate publicCert = this.caProvider.getSigningCaCertificate();
        org.junit.Assert.assertNotNull(publicCert);
        if (logger.isInfoEnabled()) {
            logger.info("Certificate found: {}", Base64.toBase64String(publicCert.getEncoded()));
        }
    }

    @org.junit.Test
    public void testCertificateChain() throws Exception {
        org.junit.Assert.assertNotNull(this.caProvider);
        X509Certificate[] chain = this.caProvider.getSignignCertificateChain();
        org.junit.Assert.assertNotNull(chain);
        org.junit.Assert.assertTrue(chain.length > 0);
        if (logger.isInfoEnabled()) {
            logger.info("Certificate chain found: {}", chain.length);
        }
    }

    @org.junit.Test
    public void testCertificateSigning() throws Exception {

        org.junit.Assert.assertNotNull(this.caProvider);
        StringBuilder nameBuilder = new StringBuilder();
        nameBuilder
                .append("C=BE,OU=TEST,CN=").append(System.currentTimeMillis());
        String subjectName = nameBuilder.toString();
        logger.info("subject: {}", subjectName);

        PemReader pemReader = new PemReader(new InputStreamReader(getClass().getResourceAsStream("/requests/test02.csr")));
        PemObject pemObject = pemReader.readPemObject();

        PKCS10CertificationRequest csr = new PKCS10CertificationRequest(pemObject.getContent());
        pemReader.close();

        X509Certificate certificate = this.caProvider.signCertificateRequest(subjectName, CaProvider.TYPE_SSL_CLIENT, csr);
        logger.info("returned certificate: {}", Base64.toBase64String(certificate.getEncoded()));
        this.describeCertificate(certificate);
        org.junit.Assert.assertNotNull(certificate);
    }
    
    
    private void describeCertificate(X509Certificate cert) {
        logger.info(cert.toString());
    }
}
