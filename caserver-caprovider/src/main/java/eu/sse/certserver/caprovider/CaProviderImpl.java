/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.sse.certserver.caprovider;

import eu.sse.certserver.common.services.CaProvider;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URL;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.security.spec.X509EncodedKeySpec;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.security.auth.x500.X500Principal;
import javax.sql.DataSource;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.ASN1Set;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DERNumericString;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.DERUTF8String;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.DistributionPoint;
import org.bouncycastle.asn1.x509.DistributionPointName;
import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.GeneralNames;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.ReasonFlags;
import org.bouncycastle.cert.X509CRLHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509ExtensionUtils;
import org.bouncycastle.cert.jcajce.JcaX509v2CRLBuilder;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.cms.CMSTypedData;
import org.bouncycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.util.Store;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.x509.X509V2CRLGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * implementation of the CA capability
 *
 *
 * to be added: init-method to validate the inputs CRL, OCSP, audit capability
 * auditing
 *
 *
 * @author Gabriel
 */
public class CaProviderImpl implements CaProvider {

    private static final Logger logger = LoggerFactory.getLogger(CaProviderImpl.class);
    private static final int VALIDITY_DEFAULT = 365;
    private static final String CERT_SIGNING_ALG = "SHA1withRSA";
//    private static final String CERT_SIGNING_ALG = "SHA256withRSA";
    private static final String SHA_1_ALG = "SHA-1";

    private static final String CONTENT_SIGNED_ISSUED = "Issued";
    private static final String id_cmc_cMCStatusInfo = "1.3.6.1.5.5.7.7.1";
    private static final String szOID_CMC_ADD_ATTRIBUTES = "1.3.6.1.4.1.311.10.10.1";
    private static final String szOID_ISSUED_CERT_HASH = "1.3.6.1.4.1.311.21.17";

    private String keystoreUrl;
    private String keystoreType;
    private String keystorePassword;
    private String keyAlias;
    private String keyAliasPasswpord;
    private Integer validity;
    /**
     * URL where the CRL endpoint is exposed
     */
    private String crlUrl;

    private javax.sql.DataSource dataSource;
    /**
     * SQL statement to get next value of a sequence used for the certificate
     * serial
     */
    private String nextValueStatement;
    private static final String SQL_INSERT_CERT_RECORD
            = "INSERT INTO ISSUED_CERTIFICATES "
            + "(ID, DN, CREATED, VALID_FROM, VALID_TO, REVOKED, HASH, CERTIFICATE) VALUES "
            + "(?, ?, ?, ?, ?, ?, ?, ?)";
//    ID bigint,
//    DN VARCHAR(255),
//    CREATED TIMESTAMP,
//    VALID_FROM TIMESTAMP,
//    VALID_TO TIMESTAMP,
//    REVOKED TIMESTAMP,
//    HASH VARCHAR(4096),
//    CERTIFICATE text);

    private static final String SQL_SELECT_REVOKED = "SELECT id, dn, revoked FROM ISSUED_CERTIFICATES WHERE REVOKED IS NOT NULL";

    /**
     * if defined, we will check if the subject DN contains the suffix
     */
    private String subjectPrefix;

    private KeyStore caKeystore;

    /**
     * initialize on attribute updates
     */
    public void init() {
        this.caKeystore = null;
        try {
            this.getCaKeyStore();
        } catch (Exception ex) {
            logger.error("init", ex);
        }
    }

    @Override
    public X509Certificate getSigningCaCertificate() {
        try {
            KeyStore keystore = this.getCaKeyStore();
            KeyStore.PrivateKeyEntry entry = (KeyStore.PrivateKeyEntry) keystore.getEntry(
                    getKeyAlias(),
                    new KeyStore.PasswordProtection(getKeystorePassword().toCharArray()));
            X509Certificate certificate = (X509Certificate) entry.getCertificate();
            return certificate;
        } catch (Exception ex) {
            logger.error("getSigningCaCertificate", ex);
            throw new RuntimeException("Unable to return the signing certificate");
        }
    }

    public X509Certificate[] getSignignCertificateChain() {
        try {
            KeyStore keystore = this.getCaKeyStore();
            KeyStore.PrivateKeyEntry entry = (KeyStore.PrivateKeyEntry) keystore.getEntry(
                    getKeyAlias(),
                    new KeyStore.PasswordProtection(getKeystorePassword().toCharArray()));
            Certificate[] certificates = entry.getCertificateChain();
            X509Certificate[] chain = new X509Certificate[certificates.length];
            System.arraycopy(certificates, 0, chain, 0, certificates.length);
            return chain;
        } catch (Exception ex) {
            logger.error("getSignignCertificateChain", ex);
            throw new RuntimeException("Unable to return the signing certificates");
        }
    }

    /**
     *
     * @return encoded CRL response
     */
    @Override
    public byte[] getCrl() {

        if (logger.isDebugEnabled()) {
            logger.debug("getCrl");
        }

        try {
            KeyStore keystore = this.getCaKeyStore();
            KeyStore.PrivateKeyEntry caPrivateKey
                    = (KeyStore.PrivateKeyEntry) keystore.getEntry(
                            getKeyAlias(),
                            new KeyStore.PasswordProtection(getKeyAliasPasswpord().toCharArray()));

            X509Certificate caCert = (X509Certificate) caPrivateKey.getCertificate();
            JcaX509ExtensionUtils extUtils = new JcaX509ExtensionUtils();

            Calendar cal = Calendar.getInstance();
            Date now = cal.getTime();
            // hardcoded update value? Parametrize?
            cal.add(1, Calendar.DATE);
            Date nextUpdate = cal.getTime();

            JcaX509v2CRLBuilder crlBuilder = new JcaX509v2CRLBuilder(caCert.getSubjectX500Principal(), now);
            
            crlBuilder
                    .setNextUpdate(nextUpdate)
                    .addExtension(Extension.authorityKeyIdentifier, false, extUtils.createAuthorityKeyIdentifier(caCert.getPublicKey()));
                    
            long crlNumber = 0L;
            try (
                    Connection dbConn = getDataSource().getConnection();
                    Statement stmt = dbConn.createStatement();) {

                ResultSet rs = stmt.executeQuery(SQL_SELECT_REVOKED);
                while (rs.next()) {
                    BigInteger serial = BigInteger.valueOf(rs.getLong("id"));
                    Timestamp revoked = rs.getTimestamp("revoked");
                    if(revoked.getTime()>crlNumber) {
                        crlNumber = revoked.getTime();
                    }

                    crlBuilder.addCRLEntry(serial, new Date(revoked.getTime()), ReasonFlags.privilegeWithdrawn);
                }
                rs.close();
            }
            
            crlBuilder.addExtension(Extension.cRLNumber, false, new ASN1Integer(crlNumber)); // timestamp of the last revoked or 0 if none
                    

            ContentSigner signer = new JcaContentSignerBuilder(CERT_SIGNING_ALG).setProvider("BC").build(caPrivateKey.getPrivateKey());
            X509CRLHolder crlHolder = crlBuilder.build(signer);
            return crlHolder.getEncoded();
        } catch (Exception ex) {
            logger.error("getCrl", ex);
            throw new RuntimeException("Unable to provide the CRL");
        }

    }

    public X509Certificate signCertificateRequest(String subject, int type, PKCS10CertificationRequest csr) {

        // this should be maybe rather audited
        if (logger.isInfoEnabled()) {
            logger.info("Certificate signing request type {} from {}", type, subject);
        }

        // validate subject
        if (subject == null || "".equals(subject)) {
            throw new IllegalArgumentException("Subject must be defined");
        }

        // at this point the service is not directly exposed
        // we will sing whatever the wstrust service implementation
        // asks for
//        if (getSubjectPrefix() != null && !"".equals(getSubjectPrefix())) {
//
//            if (!subject.toLowerCase().startsWith(getSubjectPrefix().toLowerCase())) {
//                throw new IllegalArgumentException("Subject must have prefix: " + getSubjectPrefix());
//            }
//
//        }
        // validate type
        switch (type) {
            case CaProvider.TYPE_SSL_SERVER:
            case CaProvider.TYPE_SSL_CLIENT:
            case CaProvider.TYPE_CODE_SIGNING:
                break;
            default:
                throw new IllegalArgumentException("Invalid certificate type");
        }

        try (java.sql.Connection dbConn = (this.getDataSource() != null) ? this.getDataSource().getConnection() : null) {
            // just to parse so we ensure the valid format
            X500Name subjectName = new X500Name(subject);

            // load the ca keys
            KeyStore keystore = this.getCaKeyStore();
            KeyStore.PrivateKeyEntry caPrivateKey
                    = (KeyStore.PrivateKeyEntry) keystore.getEntry(
                            getKeyAlias(),
                            new KeyStore.PasswordProtection(getKeyAliasPasswpord().toCharArray()));
            X509Certificate caCertificate = (X509Certificate) caPrivateKey.getCertificate();
            caCertificate.checkValidity();

            // parse the subject public key (assuming RSA )
            // TBD check the public key type
            X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(csr.getSubjectPublicKeyInfo().getEncoded());
            PublicKey csrPubKey = KeyFactory.getInstance("RSA").generatePublic(pubKeySpec);

            // prepare the new ca parameters
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(System.currentTimeMillis());
            Date validFrom = cal.getTime();
            cal.add(Calendar.DATE, getValidity());
            Date validTo = cal.getTime();
            BigInteger serial = getNextSerial(dbConn);

            X509v3CertificateBuilder certBldr = new JcaX509v3CertificateBuilder(
                    caCertificate.getSubjectX500Principal(),
                    serial,
                    validFrom,
                    validTo,
                    new X500Principal(subject),
                    csrPubKey);

            JcaX509ExtensionUtils extUtils = new JcaX509ExtensionUtils();
            certBldr.addExtension(Extension.authorityKeyIdentifier, false, extUtils.createAuthorityKeyIdentifier(caCertificate.getPublicKey()))
                    .addExtension(Extension.subjectKeyIdentifier, false, extUtils.createSubjectKeyIdentifier(csrPubKey))
                    .addExtension(Extension.basicConstraints, true, new BasicConstraints(false))
                    .addExtension(Extension.cRLDistributionPoints, false, createCrlDistributionPointSeq());

            switch (type) {
                case CaProvider.TYPE_SSL_SERVER:
                    certBldr
                            .addExtension(Extension.keyUsage, false, new KeyUsage(KeyUsage.keyEncipherment))
                            .addExtension(Extension.extendedKeyUsage, true, new ExtendedKeyUsage(KeyPurposeId.id_kp_serverAuth));
                    break;
                case CaProvider.TYPE_SSL_CLIENT:
                    certBldr
                            .addExtension(Extension.keyUsage, false, new KeyUsage(KeyUsage.digitalSignature))
                            .addExtension(Extension.extendedKeyUsage, true, new ExtendedKeyUsage(KeyPurposeId.id_kp_clientAuth));
                    break;
                case CaProvider.TYPE_CODE_SIGNING:
                    certBldr
                            .addExtension(Extension.keyUsage, false, new KeyUsage(KeyUsage.digitalSignature))
                            .addExtension(Extension.extendedKeyUsage, true, new ExtendedKeyUsage(KeyPurposeId.id_kp_codeSigning));
                    break;
                default:
                    throw new IllegalArgumentException("Invalid certificate type");
            }

            ContentSigner signer = new JcaContentSignerBuilder(CERT_SIGNING_ALG).setProvider("BC").build(caPrivateKey.getPrivateKey());
            X509Certificate certificate = new JcaX509CertificateConverter().setProvider("BC").getCertificate(certBldr.build(signer));

            // log certificate
            this.logCertificate(dbConn, certificate);

            return certificate;
            // TBD audit
            // TBD add the certificate to the database for revocation/
        } catch (Exception ex) {
            logger.error("signCertificateRequest", ex);
            throw new RuntimeException("Unable to sign the request");
        }
    }

    private DERSequence createCrlDistributionPointSeq() {
        GeneralName gn = new GeneralName(GeneralName.uniformResourceIdentifier, new DERIA5String(getCrlUrl()));
        GeneralNames gns = new GeneralNames(gn);
        DistributionPointName dpn = new DistributionPointName(gns);
        DistributionPoint distp = new DistributionPoint(dpn, null, null);
        DERSequence crlSeq = new DERSequence(distp);
        return crlSeq;
    }

    /**
     *
     * @param dbConn
     * @throws SQLException
     */
    private void logCertificate(Connection dbConn, X509Certificate certificate) throws SQLException, CertificateEncodingException {

        if (dbConn == null) {
            return;
        }

        try (PreparedStatement stmt = dbConn.prepareStatement(SQL_INSERT_CERT_RECORD)) {
            stmt.setLong(1, certificate.getSerialNumber().longValue());
            stmt.setString(2, certificate.getSubjectX500Principal().getName());
            stmt.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
            stmt.setTimestamp(4, new Timestamp(certificate.getNotBefore().getTime()));
            stmt.setTimestamp(5, new Timestamp(certificate.getNotAfter().getTime()));
            stmt.setNull(6, Types.TIMESTAMP);
            stmt.setString(7, Base64.toBase64String(certificate.getSignature()));
            stmt.setString(8, Base64.toBase64String(certificate.getEncoded()));
//    ID bigint,
//    DN VARCHAR(255),
//    CREATED TIMESTAMP,
//    VALID_FROM TIMESTAMP,
//    VALID_TO TIMESTAMP,
//    REVOKED TIMESTAMP,
//    HASH VARCHAR(255),
//    CERTIFICATE CLOB);         
            stmt.executeUpdate();
        }
    }

    /**
     * return pkcs7 signed data intended to sing the certificate with the whole
     * chain
     *
     * @param data
     * @return
     */
    public byte[] signCertificate(X509Certificate certificate) throws Exception {

        try {
            KeyStore ks = this.getCaKeyStore();
            KeyStore.PrivateKeyEntry pkEntry = (KeyStore.PrivateKeyEntry) ks.getEntry(getKeyAlias(), new KeyStore.PasswordProtection(getKeyAliasPasswpord().toCharArray()));

            List certList = new ArrayList();
            // NOTE: works when the root (or intermediate) certificate 
            // is not explicitly trusted (WTF ???)
            // CRYPT_E_HASH_VALUE error
            for (X509Certificate chainCert : this.getSignignCertificateChain()) {
                certList.add(chainCert); //Adding the X509 Certificate
            }
            certList.add(certificate);
            Store certs = new JcaCertStore(certList);

            // prepare message
            // this should be the whole chain
            CMSTypedData msg = new CMSProcessableByteArray(prepareCertificateHashToSign(certificate).getEncoded());

            CMSSignedDataGenerator gen = new CMSSignedDataGenerator();
            //Initializing the the BC's Signer
            ContentSigner sha256Signer = new JcaContentSignerBuilder(CERT_SIGNING_ALG).setProvider("BC").build(pkEntry.getPrivateKey());

            gen.addSignerInfoGenerator(
                    new JcaSignerInfoGeneratorBuilder(
                            new JcaDigestCalculatorProviderBuilder().setProvider("BC").build())
                    .build(sha256Signer, getSigningCaCertificate()));
            //adding the certificate
            gen.addCertificates(certs);
            //Getting the signed data
            CMSSignedData sigData = gen.generate(msg, false);
            return sigData.getEncoded();

        } catch (Exception ex) {
            logger.error("getCmsSignedData", ex);
            throw new RuntimeException("Unable to sign the data");
        }
    }

    /*

     */
    private ASN1Primitive prepareCertificateHashToSign(X509Certificate certificate) throws Exception {

//SEQUENCE (3 elem)
//    SEQUENCE (2 elem) 
//        SEQUENCE (3 elem) // cmcStatusSeq
//            INTEGER 1
//            OBJECT IDENTIFIER 1.3.6.1.5.5.7.7.1 (id-cmc-cMCStatusInfo )
//            SET (1 elem)
//                SEQUENCE (3 elem)
//                    INTEGER 0
//                    SEQUENCE (1 elem)
//                        INTEGER 1
//                    UTF8String Issued
//        SEQUENCE (3 elem)
//            INTEGER 2
//            OBJECT IDENTIFIER 1.3.6.1.4.1.311.10.10.1 (szOID_CMC_ADD_ATTRIBUTES)
//            SET (1 elem)
//                SEQUENCE (3 elem)
//                    INTEGER 0
//                    SEQUENCE (1 elem)
//                        INTEGER 1
//                    SET (1 elem)
//                        SEQUENCE (2 elem)
//                          OBJECT IDENTIFIER 1.3.6.1.4.1.311.21.17 (szOID_ISSUED_CERT_HASH)
//                          SET (1 elem)
//                              OCTET STRING (20 byte) 8A205E6C0E04EB8CD4317B006C63BB182EAC3B22
//    SEQUENCE(0 elem)
//    SEQUENCE(0 elem)  
        ASN1Encodable[] rootElements = new ASN1Encodable[3];

        // id-cmc-cMCStatusInfo
        ASN1Encodable[] encodables = new ASN1Encodable[3];
        encodables[0] = new ASN1Integer(0L);
        encodables[1] = new DERSequence(new ASN1Integer(1L));
        encodables[2] = new DERUTF8String(CONTENT_SIGNED_ISSUED);
        ASN1Sequence asn1Seq = new DERSequence(encodables);
        ASN1Set asn1Set = new DERSet(asn1Seq);
        encodables = new ASN1Encodable[3];
        encodables[0] = new ASN1Integer(1L);
        encodables[1] = new ASN1ObjectIdentifier(id_cmc_cMCStatusInfo);
        encodables[2] = asn1Set;
        ASN1Sequence cmcStatusSeq = new DERSequence(encodables);

        // szOID_CMC_ADD_ATTRIBUTES
        asn1Seq = new DERSequence(new ASN1Encodable[]{
            new ASN1ObjectIdentifier(szOID_ISSUED_CERT_HASH),
            new DERSet(new DEROctetString(computeSha1Hash(certificate.getEncoded())))
        });
        encodables = new ASN1Encodable[3];
        encodables[0] = new ASN1Integer(0L);
        encodables[1] = new DERSequence(new ASN1Integer(1L));
        encodables[2] = new DERSet(asn1Seq);
        asn1Set = new DERSet(new DERSequence(encodables));
        encodables = new ASN1Encodable[3];
        encodables[0] = new ASN1Integer(2L);
        encodables[1] = new ASN1ObjectIdentifier(szOID_CMC_ADD_ATTRIBUTES);
        encodables[2] = asn1Set;
        ASN1Sequence hashSeq = new DERSequence(encodables);

        rootElements[0] = new DERSequence(new ASN1Encodable[]{
            cmcStatusSeq, hashSeq
        });
        rootElements[1] = new DERSequence();
        rootElements[2] = new DERSequence();
        ASN1Primitive msgContent = new DERSequence(rootElements);

        return msgContent;
    }

    /**
     * return SHA1 of the provided data used to compute the
     * szOID_ISSUED_CERT_HASH
     * https://msdn.microsoft.com/en-us/library/cc249862.aspx
     */
    private byte[] computeSha1Hash(byte[] data) throws Exception {
        MessageDigest sha1md = MessageDigest.getInstance(SHA_1_ALG);
        return sha1md.digest(data);
    }

    // utilities
    private KeyStore getCaKeyStore() throws Exception {
        // TBD consider cashing the keystore (?)
        if (this.caKeystore == null) {
            this.caKeystore = this.loadKeystore();
        }
        return this.caKeystore;
    }

    private KeyStore loadKeystore() throws Exception {
        KeyStore keystore = KeyStore.getInstance(this.getKeystoreType());
        InputStream in = null;
        try {
            in = new URL(this.getKeystoreUrl()).openStream();
            keystore.load(in, getKeystorePassword().toCharArray());
        } finally {
            if (in != null) {
                in.close();
            }
        }
        return keystore;
    }

    /**
     * generated from the timestamp, should be from a persistent storage to
     * ensure uniqueness must fit into 20 bytes
     *
     * @return the next serial number
     */
    private BigInteger getNextSerial(java.sql.Connection dbConn) {

        BigInteger result = null;
        if (dbConn == null) {
            result = BigInteger.valueOf(System.currentTimeMillis());
            return result;
        }
        try (java.sql.PreparedStatement stmt = dbConn.prepareStatement(getNextValueStatement())) {
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                result = BigInteger.valueOf(rs.getLong(1));
            }
        } catch (Exception ex) {
            logger.warn("getNextSerial", ex);
            result = BigInteger.valueOf(System.currentTimeMillis());
        }
        return result;
    }

    // ------- accessors -----------
    public String getKeystoreUrl() {
        return keystoreUrl;
    }

    public void setKeystoreUrl(String keystoreUrl) {
        this.keystoreUrl = keystoreUrl;
    }

    public String getKeystoreType() {
        return keystoreType;
    }

    public void setKeystoreType(String keystoreType) {
        this.keystoreType = keystoreType;
    }

    public String getKeyAlias() {
        return keyAlias;
    }

    public void setKeyAlias(String keyAlias) {
        this.keyAlias = keyAlias;
    }

    public String getSubjectPrefix() {
        return subjectPrefix;
    }

    public void setSubjectPrefix(String subjectPrefix) {
        this.subjectPrefix = subjectPrefix;
    }

    public String getKeystorePassword() {
        return keystorePassword;
    }

    public void setKeystorePassword(String keystorePassword) {
        this.keystorePassword = keystorePassword;
    }

    public String getKeyAliasPasswpord() {
        return keyAliasPasswpord;
    }

    public void setKeyAliasPasswpord(String keyAliasPasswpord) {
        this.keyAliasPasswpord = keyAliasPasswpord;
    }

    public Integer getValidity() {
        if (validity == null || validity < 1) {
            validity = VALIDITY_DEFAULT;
        }
        return validity;
    }

    public void setValidity(Integer validity) {
        this.validity = validity;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public String getNextValueStatement() {
        return nextValueStatement;
    }

    public void setNextValueStatement(String nextValueStatement) {
        this.nextValueStatement = nextValueStatement;
    }

    public String getCrlUrl() {
        return crlUrl;
    }

    public void setCrlUrl(String crlUrl) {
        this.crlUrl = crlUrl;
    }

}
