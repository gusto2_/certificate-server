/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.sse.certserver.enrollment;

import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.Attributes;
import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.CA;
import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.CACollection;
import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.CAReferenceCollection;
import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.CAURI;
import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.CAURICollection;
import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.CertificateEnrollmentPolicy;
import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.CertificateValidity;
import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.Client;
import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.CryptoProviders;
import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.EnrollmentPermission;
import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.Extension;
import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.ExtensionCollection;
import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.IPolicy;
import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.OID;
import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.OIDCollection;
import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.PolicyCollection;
import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.PrivateKeyAttributes;
import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.RequestFilter;
import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.Response;
import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.Revision;
import eu.sse.certserver.common.services.CaProvider;
import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.cert.CertificateEncodingException;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Holder;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.handler.MessageContext;
import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TO BE DONE:
 *  - extensions / oids should be provded by the CA provider
 * 
 * @author Gabriel
 */
public class ClientCertificatePolicyService implements IPolicy {

    private static final Logger logger = LoggerFactory.getLogger(ClientCertificatePolicyService.class);


    /**
     * default expected certificate validity - 1 year
     */
    private static final long DEFAULT_EXPECTED_VALIDITY = 365L * 24L * 3600L;
    private static final long MINIMAL_KEY_LENGTH = 1024L;
    private static final String DEFAULT_CRYPTO_PROVIDER_1 = "Microsoft Enhanced Cryptographic Provider v1.0";
    private static final String DEFAULT_CRYPTO_PROVIDER_2 = "Microsoft Base Cryptographic Provider v1.0";

    private static final long DEFAULT_REVISION_MAJOR = 1L;
    private static final long DEFAULT_REVISION_MINOR = 1L;
    
    private static final int REFID_OID_TEMPLATE_NAME = 5;
    private static final int REFID_OID_KEY_USAGE = 6;
    private static final int REFID_OID_EXTENDED_KEY_USAGE = 7;
    private static final int REFID_OID_BASIC_EFS = 9;
    
    // requested extensions group (?)
     private static final int OID_GROUP_REQ = 6;
     private static final int OID_GROUP_POLICY = 9;
     private static final String OID_TEMPLATE_NAME = "Certificate Template Name";
     private static final String OID_TEMPLATE_NAME_VALUE="1.3.6.1.4.1.311.20.2";
     private static final String OID_KEY_USAGE = "Key Usage";
     private static final String OID_KEY_USAGE_VALUE="2.5.29.15";
     private static final String OID_EXT_KEY_USAGE = "Enhanced Key Usage";
     private static final String OID_EXT_KEY_USAGE_VALUE="2.5.29.37";
     private static final String OID_POLICY_NAME = "Client certificate";
     private static final String OID_POLICY_NAME_VALUE="1.3.6.1.4.1.311.21.8.3800100.3166153.13323660.9808540.8334961.78.1.6";
     
     

    @Resource
    private WebServiceContext context;

    /**
     * MS-WSTEP/WSTRUST service
     * this URL should be provided by the CA provider
     * 
     */
    private String wstrustService;

    /**
     * ranges of allowed IP addresses of the clients
     */
    private String ipAddressRanges;

    /**
     * GUID format
     */
    private String policyId;

    /**
     * policy name
     */
    private String policyFriendlyName;

    /**
     * policy update interval, by default 48h, however no updates are supported
     * atm
     */
    private Integer policyUpdateInterval;
    private static final int DEFAULT_POLICY_UPDATE_INTERVAL = 48;

    private CaProvider caProvider;

    public void getPolicies(Client client, RequestFilter requestFilter, Holder<Response> response, Holder<CACollection> cAs, Holder<OIDCollection> oIDs) {

        try {
            String clientAddr = this.getClientAddress();
            if (logger.isInfoEnabled()) {
                logger.info("Policy request from: {}", clientAddr);
            }

            XMLGregorianCalendar lastUpdate = client.getLastUpdate();
            if (lastUpdate != null) {
                // provide updates
                response.value = this.createUpdateResponse(client, requestFilter);
                return;
            } else {

                // response
                CA caRecord = this.createCaRecord(client, requestFilter);
                response.value = this.createInitialResponse(caRecord, client, requestFilter);

                // cASs
                CACollection cas = new CACollection();
                cas.getCA().add(caRecord);
                cAs.value = cas;

                // oIDs
                oIDs.value = this.createOidCollection();
                
            }
        } catch (Exception ex) {
            logger.error("getPolicies", ex);
            throw new WebServiceException("Unable to return the enrollment policies");
        }
    }
    
    private OIDCollection createOidCollection() {
        OIDCollection oids = new OIDCollection();
        
        // template name
        OID oid = new OID();
        oid.setOIDReferenceID(REFID_OID_TEMPLATE_NAME);
        oid.setGroup(OID_GROUP_REQ);
        oid.setDefaultName(OID_TEMPLATE_NAME);
        oid.setValue(OID_TEMPLATE_NAME_VALUE);
        oids.getOID().add(oid);
        // key usage
        oid = new OID();
        oid.setOIDReferenceID(REFID_OID_KEY_USAGE);
        oid.setGroup(OID_GROUP_REQ);
        oid.setDefaultName(OID_KEY_USAGE);
        oid.setValue(OID_KEY_USAGE_VALUE);
        oids.getOID().add(oid);
        // ext key usage
        oid = new OID();
        oid.setOIDReferenceID(REFID_OID_KEY_USAGE);
        oid.setGroup(OID_GROUP_REQ);
        oid.setDefaultName(OID_EXT_KEY_USAGE);
        oid.setValue(OID_EXT_KEY_USAGE_VALUE);
        oids.getOID().add(oid);
        // EFS (policy oid)
        oid = new OID();
        oid.setOIDReferenceID(REFID_OID_BASIC_EFS);
        oid.setGroup(OID_GROUP_POLICY);
        oid.setDefaultName(OID_POLICY_NAME);
        oid.setValue(OID_POLICY_NAME_VALUE);
        oids.getOID().add(oid);
        
        return oids;
    }

    private Response createInitialResponse(CA ca, Client client, RequestFilter requestFilter) throws CertificateEncodingException, IOException {
        Response response = new Response();

        response.setPolicyID(getPolicyId());
        response.setPolicyFriendlyName(getPolicyFriendlyName());
        response.setNextUpdateHours(getPolicyUpdateInterval().longValue());

        //policies
        // policy for the CA
        PolicyCollection policyList = new PolicyCollection();
        response.setPolicies(policyList);
        CertificateEnrollmentPolicy policy = new CertificateEnrollmentPolicy();
        policyList.getPolicy().add(policy);

        policy.setPolicyOIDReference(REFID_OID_BASIC_EFS);
        policy.setCAs(new CAReferenceCollection());
        policy.getCAs().getCAReference().add(ca.getCAReferenceID());

        // attributes
        Attributes attributes = new Attributes();
        policy.setAttributes(attributes);
        attributes.setCommonName(getPolicyFriendlyName());
        attributes.setPolicySchema(1L);
        CertificateValidity validity = new CertificateValidity();
        // set validity to 1 year
        validity.setValidityPeriodSeconds(BigInteger.valueOf(DEFAULT_EXPECTED_VALIDITY));
        validity.setRenewalPeriodSeconds(BigInteger.valueOf(DEFAULT_EXPECTED_VALIDITY));
        attributes.setCertificateValidity(validity);
        // enrollment permission
        EnrollmentPermission enrollmentPermission = new EnrollmentPermission();
        enrollmentPermission.setEnroll(true);
        enrollmentPermission.setAutoEnroll(true);
        attributes.setPermission(enrollmentPermission);
        // expected private key attrbiutes
        PrivateKeyAttributes pkAttributes = new PrivateKeyAttributes();
        pkAttributes.setMinimalKeyLength(MINIMAL_KEY_LENGTH);
        // keyspec, see MS-WCCE
//        Algorithm, key size when Certificate.Template.pkiDefaultKeySpec Value = 0x1 (AT_KEYEXCHANGE) 
//        Algorithm, key size when Certificate.Template.pkiDefaultKeySpec Value = 0x2 (AT_SIGNATURE) 
//        "Microsoft Base Cryptographic Provider" 
//        RSA, 512 [PKCS1] RSA, 512 [PKCS1] 
//        "Microsoft Strong Cryptographic Provider" 
//        RSA, 1024 [PKCS1] RSA, 1024 [PKCS1] 
//        "Microsoft Enhanced Cryptographic Provider" 
//        RSA, 1024 [PKCS1] RSA, 1024 [PKCS1] 
//        "Microsoft AES Cryptographic Provider" 
//        RSA, 1024 [PKCS1] RSA, 1024 [PKCS1] 
//        "Microsoft DSS Cryptographic Provider" 
//        Not available DSA, 1024 [FIPS186] 
//        "Microsoft Base DSS and DiffieHellman Cryptographic Provider" 
//        Diffie-Hellman, 512 [PKCS3] DSA, 1024 [FIPS186] 
//        "Microsoft Enhanced DSS and Diffie-Hellman Cryptographic Provider" 
//        Diffie-Hellman, 1024 [PKCS3] DSA, 1024 [FIPS186] 
//        "Microsoft DSS and DiffieHellman/Schannel Cryptographic Provider" 
//        Diffie-Hellman, 512 [PKCS3] "Not available" 
//        "Microsoft RSA/Schannel Cryptographic Provider" 
//        RSA, 1024, [PKCS1] "Not available"         
        pkAttributes.setKeySpec(0x1L);
        // key usage to be included in the certificate request
        pkAttributes.setKeyUsageProperty(null);
        //  Used to specify a Security Descriptor Definition Language (SDDL)
        // representation of the permissions when a private key is created
        pkAttributes.setPermissions(null);
//        An integer reference to an <oIDReferenceID> element of an existing OID (section 3.1.4.1.3.16) 
//        object in a GetPoliciesResponse message. The OID object that is referenced corresponds 
//        to the asymmetric algorithm of the private key. 
        pkAttributes.setAlgorithmOIDReference(null);
        attributes.setPrivateKeyAttributes(pkAttributes);
        CryptoProviders cryptoProviders = new CryptoProviders();
        cryptoProviders.getProvider().add(DEFAULT_CRYPTO_PROVIDER_1);
        cryptoProviders.getProvider().add(DEFAULT_CRYPTO_PROVIDER_2);
        pkAttributes.setCryptoProviders(cryptoProviders);
        Revision policyRevision = new Revision();
        policyRevision.setMajorRevision(DEFAULT_REVISION_MAJOR);
        policyRevision.setMinorRevision(DEFAULT_REVISION_MINOR);
        attributes.setRevision(policyRevision);
        // WCCE 3.1.2.4.2.2.2.8 - 0x00000020L = strong encryption is required
        // according to the samples, this can be null
        attributes.setPrivateKeyFlags(0x00000020L);
//        0x00000001 The client supplies the Subject field value in the certificate request. 
//        0x00010000 The client supplies the Subject Alternative Name field value in the certificate request. 
//        0x00400000 The certificate authority (CA) adds the value of the DNS of the root domain (the domain where the user's o
//          bject resides in Active Directory) to the Subject Alternative Name extension of the issued certificate. 
//        0x00800000 The CA adds the value of the userPrincipalName attribute from the requestor's user object 
//          in Active Directory to the Subject Alternative Name extension of the issued certificate. 
//        0x01000000 The CA adds the value of the objectGUID attribute from the requestor's user object 
//          in Active Directory to the Subject Alternative Name extension of the issued certificate. 
//        0x02000000 The CA adds the value of the userPrincipalName attribute from the requestor's user object 
//          in Active Directory to the Subject Alternative Name extension of the issued certificate. 
//        0x04000000 The CA adds the value of the mail attribute from the requestor's user object 
//          in Active Directory to the Subject Alternative Name extension of the issued certificate. 
//        0x08000000 The CA adds the value obtained from the dNSHostName attribute of the requestor's user object 
//          in Active Directory to the Subject Alternative Name extension of the issued certificate. 
//        0x10000000 The CA adds the value obtained from the dNSHostName attribute of the requestor's user object 
//          in Active Directory as the CN in the Subject extension of the issued certificate. 
//        0x20000000 The CA adds the value of the mail attribute from the requestor's user object 
//          in Active Directory as the Subject extension of the issued certificate. 
//        0x40000000 The CA sets the Subject Name to the cn attribute value of the requestor's user object in Active Directory. 
//        0x80000000 The CA sets the Subject Name to the distinguishedName attribute value of the requestor's user object in Active Directory. 
//        0x00000008 The client reuses the values of the Subject Name and Subject Alternative Name extensions from an existing, valid certificate 
//          when creating a renewal certificate request. This flag can only be used when the SubjectNameEnrolleeSupplies (0x00000001) 
//          or SubjectAlternativeNameEnrolleeSupplies (0x00010000) flag is specified
        attributes.setSubjectNameFlags(null);
//        0x00000001 Instructs the client and CA to include an S/MIME extension, as specified in [RFC4262]. 
//        0x00000008 Instructs the CA to append the issued certificate to the userCertificate attribute, 
//          on the user object in Active Directory. 
//        0x00000010 Instructs the CA to check the user's userCertificate attribute in Active Directory,
//           as specified in [RFC4523], for valid certificates that match the template enrolled for. 
//        0x00000040 This flag instructs clients to sign the renewal request using the private key of the existing certificate. 
//          For more information, see [MS-WCCE] section 3.2.2.6.2.1.4.5.6. This flag also instructs the CA to process the renewal requests
//          as specified in [MS-WCCE] section 3.2.2.6.2.1.4.5.6. 
//        0x00000100 Instructs the client to get a user's consent before attempting to enroll for a certificate based on the specified template. 
//        0x00000400 Instructs the client to delete any expired, revoked, or renewed certificate from the user's certificate stores. 
//        0x00002000 This flag instructs the client to reuse the private key for a smart card–based certificate renewal if it
//          is unable to create a new private key on the card. 
        // can be  null
        attributes.setEnrollmentFlags(0x00000100L);
//        0x00000040 GeneralMachineType This certificate template is for an end entity that represents a machine. 
//        0x00000080 GeneralCA A certificate request for a CA certificate. 
//        0x00000800 GeneralCrossCA A certificate request for cross-certifying a certificate. 
        attributes.setGeneralFlags(null);
        // here we can enforce stronger signing hash
        attributes.setHashAlgorithmOIDReference(null);
        // if additional signatures (cross-signing?) is required
        attributes.setRARequirements(null);
        //  required attributes that MUST be used on the client prior to sending 
        // the client private key to the server for archival
        attributes.setKeyArchivalAttributes(null);
        // extensions
        ExtensionCollection extensions = this.createExceptions(ca, client, requestFilter);
        attributes.setExtensions(extensions);
        
        return response;
    }
    
    /**
     * CSR extensions, these should be read from the CA provider
     * 
     * for the client certificates, we will atm set KU=DS and EKU=ClientAuth
     * 
     * @param ca
     * @param client
     * @param requestFilter
     * @return 
     */
    private ExtensionCollection createExceptions(CA ca, Client client, RequestFilter requestFilter) throws IOException {
        ExtensionCollection extensions = new ExtensionCollection();
//                            .addExtension(Extension.keyUsage, false, new KeyUsage(KeyUsage.digitalSignature))
//                            .addExtension(Extension.extendedKeyUsage, true, new ExtendedKeyUsage(KeyPurposeId.id_kp_clientAuth));
        
        
        // key usage
        Extension keyUsageExt = new Extension();
        extensions.getExtension().add(keyUsageExt);
        keyUsageExt.setOIDReference(REFID_OID_KEY_USAGE);
        keyUsageExt.setCritical(false);
        keyUsageExt.setValue(new KeyUsage(KeyUsage.digitalSignature).toASN1Primitive().getEncoded());
        
        // extended key usage
        Extension extkeyUsageExt = new Extension();
        extensions.getExtension().add(extkeyUsageExt);
        extkeyUsageExt.setOIDReference(REFID_OID_EXTENDED_KEY_USAGE);
        extkeyUsageExt.setCritical(true);
        extkeyUsageExt.setValue(new ExtendedKeyUsage(KeyPurposeId.id_kp_clientAuth).toASN1Primitive().getEncoded());
        
        return extensions;
    }
    

    private CA createCaRecord(Client client, RequestFilter requestFilter) throws CertificateEncodingException {
        CA ca = new CA();
        ca.setEnrollPermission(true);
        ca.setCAReferenceID(0);
        ca.setCertificate(getCaProvider().getSigningCaCertificate().getEncoded());
        CAURICollection uris = new CAURICollection();
        ca.setUris(uris);
        CAURI uri = new CAURI();

//        1 Anonymous Authentication 
//        2 Transport Kerberos Authentication
//        4 Message Username and Password Authentication 
//        8 Message X.509 Certificate Authentication
        // username token
        uri.setClientAuthentication(4);
        uri.setRenewalOnly(false);
        // TBD compute proper URL
        uri.setUri(this.getWstrustService());
        uris.getCAURI().add(uri);
        // get the certificate from the reference

        return ca;
    }

    /**
     * create a response for the request at the moment we support no updates
     *
     * @param client
     * @param requestFilter
     * @return
     */
    private Response createUpdateResponse(Client client, RequestFilter requestFilter) {

        Response response = new Response();
        response.setPolicyID(getPolicyId());
        response.setPolicyFriendlyName(getPolicyFriendlyName());
        response.setPoliciesNotChanged(Boolean.TRUE);
        response.setNextUpdateHours(getPolicyUpdateInterval().longValue());

        return response;
    }

    /**
     * temporary testing method if we can reliably find the client address
     *
     * @return
     */
    private String getClientAddress() throws UnknownHostException {
        if (this.getContext() == null) {
            logger.warn("service context is null");
            return null;
        }

        HttpServletRequest servletRequest = (HttpServletRequest) this.getContext().getMessageContext().get(MessageContext.SERVLET_REQUEST);
        if (servletRequest == null) {
            logger.warn("Unable to get the servlet request from the message context");
        }

        String remoteAddress = servletRequest.getRemoteAddr();
        String remoteHost = servletRequest.getRemoteHost();

        // TBS enfroce client IP range
        if (remoteHost.equals(remoteAddress)) {
            remoteHost = InetAddress.getByName(remoteHost).getCanonicalHostName();
        }

        if (logger.isInfoEnabled()) {
            logger.info("Remote address: {}", remoteAddress);
            logger.info("Remote host: {}", remoteHost);
        }

        return servletRequest.getRemoteAddr();
    }

    // ------ accessors --------

    public String getWstrustService() {
        return wstrustService;
    }

    public void setWstrustService(String wstrustService) {
        this.wstrustService = wstrustService;
    }
    

    public String getIpAddressRanges() {
        return ipAddressRanges;
    }

    public void setIpAddressRanges(String ipAddressRanges) {
        this.ipAddressRanges = ipAddressRanges;
    }

    public String getPolicyId() {
        return policyId;
    }

    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    public String getPolicyFriendlyName() {
        return policyFriendlyName;
    }

    public void setPolicyFriendlyName(String policyFriendlyName) {
        this.policyFriendlyName = policyFriendlyName;
    }

    public Integer getPolicyUpdateInterval() {
        if (this.policyUpdateInterval == null) {
            this.policyUpdateInterval = DEFAULT_POLICY_UPDATE_INTERVAL;
        }
        return policyUpdateInterval;
    }

    public void setPolicyUpdateInterval(Integer policyUpdateInterval) {
        this.policyUpdateInterval = policyUpdateInterval;
    }

    public WebServiceContext getContext() {
        return context;
    }

    public void setContext(WebServiceContext context) {
        this.context = context;
    }

    public CaProvider getCaProvider() {
        return caProvider;
    }

    public void setCaProvider(CaProvider caProvider) {
        this.caProvider = caProvider;
    }

}
