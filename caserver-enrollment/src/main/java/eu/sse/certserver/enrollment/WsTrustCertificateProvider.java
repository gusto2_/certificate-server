/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.sse.certserver.enrollment;

import com.microsoft.schemas.windows.pki._2009._01.enrollment.DispositionMessageType;
import eu.sse.certserver.common.services.CaProvider;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.cert.X509Certificate;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.util.encoders.Base64;
import org.oasis_open.docs.ws_sx.ws_trust._200512.RequestSecurityTokenResponseCollectionType;
import org.oasis_open.docs.ws_sx.ws_trust._200512.RequestSecurityTokenResponseType;
import org.oasis_open.docs.ws_sx.ws_trust._200512.RequestSecurityTokenType;
import org.oasis_open.docs.ws_sx.ws_trust._200512.RequestedSecurityTokenType;
import org.oasis_open.docs.ws_sx.ws_trust._200512.SecurityTokenService;
import org.oasis_open.docs.wss._2004._01.oasis_200401_wss_wssecurity_secext_1_0.BinarySecurityTokenType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * implementation of the wstrust provider
 *
 * @author Gabriel
 */
public class WsTrustCertificateProvider implements SecurityTokenService {

    private static final Logger logger = LoggerFactory.getLogger(WsTrustCertificateProvider.class);
    private static final String MS_WSTRUST_NAMESPACE = "http://docs.oasis-open.org/ws-sx/ws-trust/200512";
    private static final QName QNAME_WST_TOKEN_TYPE = new QName(MS_WSTRUST_NAMESPACE, "TokenType");
    private static final QName QNAME_WST_REQUEST_TYPE = new QName(MS_WSTRUST_NAMESPACE, "RequestType");
    private static final QName QNAME_WST_BINARY_SECURITY_TOKEN = new QName("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "BinarySecurityToken");
    private static final QName QNAME_WST_REQUESTED_TOKEN = new QName(MS_WSTRUST_NAMESPACE, "RequestedSecurityToken");
    private static final QName QNAME_MS_DISPOSITION_MESSAGE = new QName("http://schemas.microsoft.com/windows/pki/2009/01/enrollment", "DispositionMessage");
    private static final QName QNAME_MS_REQUEST_ID = new QName("http://schemas.microsoft.com/windows/pki/2009/01/enrollment", "RequestID");
    private static final String TOKEN_TYPE_X509_V3 = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3";
    private static final String REQUEST_TYPE_ISSUE = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/Issue";
    private static final String BINARY_TOKEN_VALUE_TYPE_PKCS10 = "http://schemas.microsoft.com/windows/pki/2009/01/enrollment#PKCS10";
    private static final String BINARY_TOKEN_VALUE_TYPE_PKCS7 = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd#PKCS7";
    private static final String BINARY_TOKEN_ENCODING_TYPE_BASE64 = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd#base64binary";

    @Resource
    private WebServiceContext serviceContext;

    private CaProvider caProvider;
    /**
     * running from the unit test doesn't require the user principal
     */
    private boolean testMode = false;
    
    /**
     * should be persistent and clustered
     */
    private int requestIdCounter = 1;
    
    /**
     * client certificate dn suffix
     */
    private String dnSuffix;

    public RequestSecurityTokenResponseCollectionType requestSecurityToken2(RequestSecurityTokenType request) {

        RequestSecurityTokenResponseCollectionType  responses = null;
        
        logger.info("remote user: {}", getSubject());

        String requestTokenType = null;
        String requestType = null;
        String requestTokenValueType = null;
        String requestTokenEncodingType = null;
        String requestValue = null;

        for (Object o : request.getAny()) {
            if (o instanceof JAXBElement) {
                JAXBElement element = (JAXBElement) o;
                QName elementName = element.getName();
                logger.info("element name: {}, value: {}, class: {}", element.getName(), element.getValue(), element.getDeclaredType());
                // token type
                if (QNAME_WST_TOKEN_TYPE.equals(elementName)) {
                    requestTokenType = (String) element.getValue();
                } // request type
                else if (QNAME_WST_REQUEST_TYPE.equals(elementName)) {
                    requestType = (String) element.getValue();
                } // binary token
                else if (QNAME_WST_BINARY_SECURITY_TOKEN.equals(elementName)) {
                    BinarySecurityTokenType requestToken = (BinarySecurityTokenType) element.getValue();
                    requestTokenValueType = requestToken.getValueType();
                    requestTokenEncodingType = requestToken.getEncodingType();
                    requestValue = requestToken.getValue();
                }

            } else {
                logger.warn("found unexpected type: {}", String.valueOf(o));
            }
        }

        if (!validateRequest(requestTokenType, requestType, requestTokenValueType, requestTokenEncodingType, requestValue)) {
            throw new IllegalArgumentException("Invalid request arguments");
        }

        // subject
        StringBuilder dnBuilder = new StringBuilder();
        dnBuilder
                .append("CN=")
                .append(getSubject())
                .append('@')
                .append(getSubjectAddr());
        if(getDnSuffix()!=null && !"".equals(getDnSuffix())) {
            dnBuilder.append(',').append(getDnSuffix());
        }

        // parse the pkcs10 request
        org.bouncycastle.pkcs.PKCS10CertificationRequest csr = null;
        try {
            csr = new PKCS10CertificationRequest(Base64.decode(requestValue));

            X509Certificate certificate = this.getCaProvider().signCertificateRequest(dnBuilder.toString(), CaProvider.TYPE_SSL_CLIENT, csr);
            if (logger.isInfoEnabled()) {
                logger.info("certificate returned: {}", certificate.toString());
            }

            responses = new RequestSecurityTokenResponseCollectionType();
            RequestSecurityTokenResponseType response = new RequestSecurityTokenResponseType();
            responses.getRequestSecurityTokenResponse().add(response);
            JAXBElement element = null;
            // token type
            element = new JAXBElement(QNAME_WST_TOKEN_TYPE, String.class, TOKEN_TYPE_X509_V3);
            response.getAny().add(element);
            // DispositionMessage
            DispositionMessageType dispositionMessage = new DispositionMessageType();
            dispositionMessage.setValue("Issued");
            element = new JAXBElement(QNAME_MS_DISPOSITION_MESSAGE, DispositionMessageType.class, dispositionMessage);
            response.getAny().add(element);
            
            // BinarySecurityToken - whole chain as pkcs7
            BinarySecurityTokenType pkcs7binaryToken = new BinarySecurityTokenType();
            pkcs7binaryToken.setValueType(BINARY_TOKEN_VALUE_TYPE_PKCS7);
            pkcs7binaryToken.setEncodingType(BINARY_TOKEN_ENCODING_TYPE_BASE64);
            byte[] pkcs7SignedValue = getCaProvider().signCertificate(certificate);
            pkcs7binaryToken.setValue(Base64.toBase64String(pkcs7SignedValue));
            element = new JAXBElement(QNAME_WST_BINARY_SECURITY_TOKEN, BinarySecurityTokenType.class, pkcs7binaryToken);
            response.getAny().add(element);
            
            // requested security token
            RequestedSecurityTokenType requestedTokenHolder = new RequestedSecurityTokenType();
            element = new JAXBElement(QNAME_WST_REQUESTED_TOKEN, RequestedSecurityTokenType.class, requestedTokenHolder);
            response.getAny().add(element);
            BinarySecurityTokenType x509ResponseToken = new BinarySecurityTokenType();
            x509ResponseToken.setEncodingType(BINARY_TOKEN_ENCODING_TYPE_BASE64);
            x509ResponseToken.setValueType(TOKEN_TYPE_X509_V3);
            x509ResponseToken.setValue(Base64.toBase64String(certificate.getEncoded()));
            element = new JAXBElement(QNAME_WST_BINARY_SECURITY_TOKEN, BinarySecurityTokenType.class, x509ResponseToken);
            requestedTokenHolder.setAny(element);
            
            // RequestId
            
            element = new JAXBElement(QNAME_MS_REQUEST_ID, String.class, String.valueOf(getNextRequestId()));
            response.getAny().add(element);
                    
        } catch (Exception ex) {
            logger.error("Unable to provide response", ex);
            throw new IllegalArgumentException("Unable to provide response");
        }
        return responses;
    }

    /**
     * return true if values are supported
     *
     * @param requestTokenType
     * @param requestType
     * @param requestTokenValueType
     * @param requestTokenEncodingType
     * @param requestValue
     * @return
     */
    private boolean validateRequest(
            String requestTokenType,
            String requestType,
            String requestTokenValueType,
            String requestTokenEncodingType,
            String requestValue) {

        boolean supported = true;
        if (requestTokenType == null || !TOKEN_TYPE_X509_V3.equals(requestTokenType)) {
            supported = false;
            logger.warn("Unsupported request token type: {}", requestTokenType);
        }

        if (requestType == null || !REQUEST_TYPE_ISSUE.equals(requestType)) {
            supported = false;
            logger.warn("Unsupported request type: {}", requestType);
        }

        if (requestTokenValueType == null || !BINARY_TOKEN_VALUE_TYPE_PKCS10.equals(requestTokenValueType)) {
            supported = false;
            logger.warn("Unsupported token value type: {}", requestType);
        }

        if (requestTokenEncodingType == null || !BINARY_TOKEN_ENCODING_TYPE_BASE64.equals(requestTokenEncodingType)) {
            supported = false;
            logger.warn("Unsupported token encoding type: {}", requestType);
        }

        if (requestValue == null) {
            supported = false;
            logger.warn("Empty request value");
        }

        return supported;
    }
    
    private int getNextRequestId() {
        int id = 0;
        synchronized(this) {
            id = this.requestIdCounter++;
        }
        return id;
    }

    private String getSubject() {
        if (this.serviceContext == null) {
            logger.warn("Context is null");
            return null;
        }

        if (this.serviceContext.getUserPrincipal() == null && !isTestMode()) {
            logger.error("User principal is null");
            throw new SecurityException("Null user principal");
        }
        return this.serviceContext.getUserPrincipal().getName();
    }

    private String getSubjectAddr() {
        if (this.serviceContext == null) {
            logger.warn("Context is null");
            return null;
        }
        
        
        HttpServletRequest  request = ((HttpServletRequest) (getServiceContext().getMessageContext()).get(MessageContext.SERVLET_REQUEST));
        String requestHost = request.getRemoteHost();
        String proxyHeaderHost = request.getHeader("x-forwarded-for");
        String resolvedHost = null;
        try {
         resolvedHost = InetAddress.getByName(request.getRemoteAddr()).getHostName();
        }
        catch(UnknownHostException uhe) {
            logger.warn("Unable to resolve: {}", uhe);
        }
        
        if(logger.isInfoEnabled()) {
            logger.info("requestHost: {}, proxyHeaderHost: {}, resolvedHost: {}", requestHost, proxyHeaderHost, resolvedHost);
        }
        
        if(resolvedHost==null) {
            resolvedHost = requestHost;
        }
        
        return resolvedHost;
    }

    public CaProvider getCaProvider() {
        return caProvider;
    }

    public void setCaProvider(CaProvider caProvider) {
        this.caProvider = caProvider;
    }

    public WebServiceContext getServiceContext() {
        return serviceContext;
    }

    public void setServiceContext(WebServiceContext serviceContext) {
        this.serviceContext = serviceContext;
    }

    public boolean isTestMode() {
        return testMode;
    }

    public void setTestMode(boolean testMode) {
        this.testMode = testMode;
    }

    public String getDnSuffix() {
        return dnSuffix;
    }

    public void setDnSuffix(String dnSuffix) {
        this.dnSuffix = dnSuffix;
    }
    
    

}
