
package eu.sse.certserver.enrollment;

import eu.sse.certserver.common.services.CaProvider;
import java.io.ByteArrayInputStream;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * provides the CRL file 
 * implemented as a REST service
 * @author Gabriel
 */

public class CRLProviderService {
   
    private static final Logger logger = LoggerFactory.getLogger(CRLProviderService.class);
    
    private CaProvider caProvider;
    
    @GET @Path("/CaServer.crl") //@javax.ws.rs.Produces("application/pkix-crl")
    public Response getCrl() {
        byte[] crlResp = getCaProvider().getCrl();
        return Response.ok().type("application/pkix-crl").entity(new ByteArrayInputStream(crlResp)).build();
    }

    public CaProvider getCaProvider() {
        return caProvider;
    }

    public void setCaProvider(CaProvider caProvider) {
        this.caProvider = caProvider;
    }

    
    
}
