
package eu.sse.certserver.enrollment;

import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.CACollection;
import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.Client;
import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.OIDCollection;
import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.RequestFilter;
import com.microsoft.schemas.windows.pki._2009._01.enrollmentpolicy.Response;
import eu.sse.certserver.caprovider.CaProviderImpl;
import java.security.Security;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Holder;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Gabriel
 */
public class ClientCertPolicyTest {
    
    private static final Logger logger = LoggerFactory.getLogger(ClientCertPolicyTest.class);
    
    private ClientCertificatePolicyService clientCertPolicy;

    private static final String KEYSTORE_URL = "file:../caserver-caprovider/src/main/resources/keystores/testrootca.jks";
    private static final String KEYSTORE_PASSWORD = "aStupid123";
    private static final String KEYSTORE_TYPE = "JKS";
    private static final String KEYSTORE_KEY_ALIAS = "testrootca";
    private static final String KEYSTORE_KEY_PASSWORD = "aStupid123";

    private static final String DN_PREFIX = "C=BE";
    private CaProviderImpl caProvider;

    
    @org.junit.Before
    public void setup() {
        
        Security.addProvider(new BouncyCastleProvider());
        
        this.caProvider = new CaProviderImpl();
        // initialize the provider
        this.caProvider.setKeystoreUrl(KEYSTORE_URL);
        this.caProvider.setKeystorePassword(KEYSTORE_PASSWORD);
        this.caProvider.setKeystoreType(KEYSTORE_TYPE);
        this.caProvider.setKeyAlias(KEYSTORE_KEY_ALIAS);
        this.caProvider.setKeyAliasPasswpord(KEYSTORE_KEY_PASSWORD);
        this.caProvider.setSubjectPrefix(DN_PREFIX);        
        
        this.clientCertPolicy = new ClientCertificatePolicyService();
        this.clientCertPolicy.setWstrustService("https://localhost:9443/cxf/wstrust/SecurityTokenService");
        this.clientCertPolicy.setIpAddressRanges("0.0.0.0/0");
        this.clientCertPolicy.setPolicyId("{1cf1b7dd-0a90-4fad-8cae-6319d4c602cc}");
        this.clientCertPolicy.setPolicyFriendlyName("TestPolicy");
        this.clientCertPolicy.setCaProvider(caProvider);
   } 
    
    @org.junit.Test
    public void testUpdateGetPolicies() throws Exception {
        Client client = new Client();
        RequestFilter requestFilter = new RequestFilter();
        Holder<Response> response = new Holder<Response>();
        Holder<CACollection> cAs = new Holder<CACollection>();
        Holder<OIDCollection> oIDs = new Holder<OIDCollection>();
        
        GregorianCalendar gcal = new GregorianCalendar();
        gcal.setTime(new Date());
        gcal.add(GregorianCalendar.DATE, -1);
        XMLGregorianCalendar xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcal);
        client.setLastUpdate(xmlCal);
        
        this.clientCertPolicy.getPolicies(client, requestFilter, response, cAs, oIDs);
        
        org.junit.Assert.assertNotNull(response.value);
        
    }
    
    @org.junit.Test
    public void testInitialGetPolicies() throws Exception {
        
        Client client = new Client();
        RequestFilter requestFilter = new RequestFilter();
        Holder<Response> response = new Holder<Response>();
        Holder<CACollection> cAs = new Holder<CACollection>();
        Holder<OIDCollection> oIDs = new Holder<OIDCollection>();
//        this.clientCertPolicy.getPolicies(client, requestFilter, response, cAs, oIDs);
        
//        // validate  the response
//        org.junit.Assert.assertNotNull(response.value);
//        
//        // validate CAs
//        org.junit.Assert.assertNotNull(cAs.value);
//        org.junit.Assert.assertNotNull(cAs.value.getCA());
//        org.junit.Assert.assertTrue(cAs.value.getCA().size()>0);
//        
//        // validate oids
//        org.junit.Assert.assertNotNull(oIDs.value);
//        org.junit.Assert.assertNotNull(oIDs.value.getOID());
    }
}
