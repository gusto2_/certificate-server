/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.sse.certserver.enrollment;

import java.security.cert.CertificateFactory;
import org.bouncycastle.asn1.x509.Certificate;
import org.bouncycastle.asn1.x509.ExtendedKeyUsage;
import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.cms.PKCS7ProcessableObject;
import org.bouncycastle.util.encoders.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Gabriel
 */
public class PrimitiveTests {
    private static final Logger logger = LoggerFactory.getLogger(PrimitiveTests.class);
    
    @org.junit.Test
    public void testKeyUsageEncoding() throws Exception {
        
        byte[] keyUsge = new KeyUsage(KeyUsage.digitalSignature).toASN1Primitive().getEncoded();
        org.junit.Assert.assertNotNull(keyUsge);
        logger.info("encoded keyUsage: {}", Base64.toBase64String(keyUsge));
    }
    
    
    
    @org.junit.Test
    public void testExtendedKeyUsageEncoding() throws Exception {
        
        byte[] extKeyUsage = new ExtendedKeyUsage(KeyPurposeId.id_kp_clientAuth).toASN1Primitive().getEncoded();
        org.junit.Assert.assertNotNull(extKeyUsage);
        logger.info("encoded extKeyUsage: {}", Base64.toBase64String(extKeyUsage));
    }
    

}
