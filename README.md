# README #

Apache ServiceMix application enabling to generate the SSL certificates using the KeyGen HTML tag and enrollment (SCEP?) services

Version:
 Apache 2 

### What is this repository for? ###

* Quick summary


* Version
0.1-SNAPSHOT still under the development

### How do I get set up? ###

* Summary of set up

install the Apache ServiceMix, install the BC provier
create root ca, create issuing ca


* Configuration

AD user repository configuration
```
<?xml version="1.0" encoding="UTF-8"?>
<blueprint xmlns="http://www.osgi.org/xmlns/blueprint/v1.0.0"
  xmlns:jaas="http://karaf.apache.org/xmlns/jaas/v1.0.0"
  xmlns:ext="http://aries.apache.org/blueprint/xmlns/blueprint-ext/v1.0.0">

	<jaas:config name="certserver_ad" rank="1">

		<jaas:module className="org.apache.karaf.jaas.modules.ldap.LDAPLoginModule"
                 flags="required">
      initialContextFactory = com.sun.jndi.ldap.LdapCtxFactory
      connection.username = techuser
      connection.password = techpassword
      connection.url = ldap://adhost:389
	  context.java.naming.referral=follow
      user.base.dn = dc=domain,dc=local
      user.filter = (sAMAccountName=%u)
      user.search.subtree = true
      role.base.dn = dc=domain,dc=local
      role.name.attribute = cn
      role.filter = (member=%dn)
      role.search.subtree = true
      authentication = simple
		</jaas:module>

	</jaas:config>


</blueprint>
```

known limitation:
 - a simple username is used for authentication, works with the current LDAPModule
 - unable to distinguish between multiple subdomains

Can we make it work with the Kerberos SSO? Otherwise the username / password 
could be logged with the cxf username token authentication



* Dependencies
* Database configuration

PGSQL
```

 CREATE DATABASE caserver;
 CREATE ROLE xxxxx WITH PASSWORD 'xxxxx' LOGIN;

 CREATE SEQUENCE serial_id no cycle;

 CREATE TABLE ISSUED_CERTIFICATES (
    ID bigint,
    DN VARCHAR(255),
    CREATED TIMESTAMP,
    VALID_FROM TIMESTAMP,
    VALID_TO TIMESTAMP,
    REVOKED TIMESTAMP,
    HASH VARCHAR(4096),
    CERTIFICATE TEXT);

 GRANT ALL ON TABLE ISSUED_CERTIFICATES TO xxxxx;
 GRANT ALL ON SEQUENCE serial_id TO xxxxx;

```

install bundle:
 - mvn:org.apache.servicemix.bundles/org.apache.servicemix.bundles.commons-dbcp/1.4_3

jdbc service:

 
```

<?xml version="1.0" encoding="UTF-8"?>
<blueprint xmlns="http://www.osgi.org/xmlns/blueprint/v1.0.0">

    <bean id="dataSource" class="org.apache.commons.dbcp.BasicDataSource">

        <property name="username" value="xxxxx"/>
		<property name="password" value="xxxxx"/>
		<property name="url" value="jdbc:postgresql://dbserver:5432/caserver"/>
        <property name="driverClassName" value="org.postgresql.Driver" />
    </bean>
 
    <service ref="dataSource" interface="javax.sql.DataSource">
        <service-properties>
            <entry key="osgi.jndi.service.name" value="/jdbc/caserver"/>
        </service-properties>
    </service>


</blueprint>

```


* How to run tests
* Deployment instructions

### Properties ###



### Revocation ###

 - update the revoked timestamp in the database (no UI yet)


### To be done ###

 - client validation (lookup of the client information, ensuring it's the domain workstation who asks the certificate)
 - Kerberos authentication (or is the basic enough?) mainly the credentials may not be logged

