/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.sse.certserver.common.services;

import java.security.cert.X509Certificate;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;

/**
 * interface providing CA services (signing, CA certificate, ...)
 * 
 * to be added: CRL, OCSP, audit capability
 * 
 * @author Gabriel
 */
public interface CaProvider {
    
    public static final int TYPE_SSL_SERVER = 0;
    public static final int TYPE_SSL_CLIENT = 1;
    public static final int TYPE_CODE_SIGNING = 2;
    
    
    public X509Certificate getSigningCaCertificate();
    
    
    public X509Certificate[] getSignignCertificateChain();
    
    
    public X509Certificate signCertificateRequest(
            String subject,
            int type, 
            PKCS10CertificationRequest csr);
    
    /**
     * return PKCS7 signed data, used to return signed certificate with the whole chain
     * 
     * @param data
     * @return 
     */
    public byte[] signCertificate(X509Certificate certificate) throws Exception;
    
    public byte[] getCrl();
    
}
