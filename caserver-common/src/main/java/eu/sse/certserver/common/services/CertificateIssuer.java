
package eu.sse.certserver.common.services;

/**
 * 
 * @author Gabriel
 */
public interface CertificateIssuer {
    
    CertificateIssuerResponse generateCertificate(CertificateIssuerRequest req) throws Exception;
    
}
