
package eu.sse.certserver.common.services;

/**
 *
 * @author Gabriel
 */
public class CertificateIssuerRequest implements java.io.Serializable {
    
    private String username;
    private byte[] certificateRequest;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public byte[] getCertificateRequest() {
        return certificateRequest;
    }

    public void setCertificateRequest(byte[] certificateRequest) {
        this.certificateRequest = certificateRequest;
    }
    
    
    
}
